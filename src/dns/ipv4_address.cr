class DNS::IPv4Address
  private property slice : Bytes

  def initialize(@slice : Bytes)
    raise "bad slice size" unless slice.size == 4
  end

  def initialize(str : String)
    @slice = str.split(".").map(&.to_u8).to_unsafe.to_slice 4
  end

  def bytesize
    @slice.size
  end

  def to_s(io : IO)
    @slice.join(".").to_s(io)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    io.write @slice
  end

  def to_slice
    @slice
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    slice = Bytes.new 4
    io.read slice
    self.new slice
  end
end
