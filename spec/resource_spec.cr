require "spec"
require "../src/dns/rr"
include DNS::RR

describe Record do
  describe RecordType::A do
    it "serializes" do
      io = IO::Memory.new
      rr = Record.new data: IPv4Address.new("1.2.3.4"), record_type: RecordType::A
      rr.to_io io, IO::ByteFormat::BigEndian
      io.rewind
      rr2 = Record.from_io io, IO::ByteFormat::BigEndian
      rr2.record_type.should eq RecordType::A
      rr2.data.as(IPv4Address).to_s.should eq "1.2.3.4"
    end
  end

  describe RecordType::AAAA do
    it "serializes" do
      io = IO::Memory.new
      rr = Record.new data: IPv6Address.new("2606:2800:0220:0001:0248:1893:25c8:1946"), record_type: RecordType::AAAA
      rr.to_io io, IO::ByteFormat::BigEndian
      io.rewind
      rr2 = Record.from_io io, IO::ByteFormat::BigEndian
      rr2.record_type.should eq RecordType::AAAA
      rr2.data.as(IPv6Address).to_s.should eq "2606:2800:0220:0001:0248:1893:25c8:1946"
    end
  end

  describe RecordType::MX do
    it "serializes" do
      io = IO::Memory.new
      mx = RR::MX.new exchange: Name.new("example.com"), preference: 10_u16
      rr = Record.new data: mx, record_type: RecordType::MX
      rr.to_io io, IO::ByteFormat::BigEndian
      io.rewind
      rr2 = Record.from_io io, IO::ByteFormat::BigEndian
      rr2.record_type.should eq RecordType::MX
      rr2.data.as(RR::MX).preference.should eq 10
    end
  end

  describe RecordType::SOA do
    it "serializes" do
      io = IO::Memory.new
      soa = RR::SOA.new mname: Name.new("example.com")
      rr = Record.new data: soa, record_type: RecordType::SOA
      rr.to_io io, IO::ByteFormat::BigEndian
      io.rewind
      rr2 = Record.from_io io, IO::ByteFormat::BigEndian
      rr2.record_type.should eq RecordType::SOA
      rr2.data.as(RR::SOA).mname.to_s.should eq "example.com"
    end
  end
end
